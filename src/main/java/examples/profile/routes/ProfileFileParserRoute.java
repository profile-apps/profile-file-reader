package examples.profile.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ProfileFileParserRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		from("file:{{input.directory}}?fileName={{file.name}}")
			.split()
			.tokenize("\n",true, true)
			.streaming()
			.to("direct:process-line");

		from("direct:process-line")
				.to("activemq:{{profile.queue.name}}");

	}

}
